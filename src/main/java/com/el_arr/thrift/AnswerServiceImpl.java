package com.el_arr.thrift;

import org.apache.thrift.TException;

public class AnswerServiceImpl implements AnswerService.Iface {
	private int key = 0;

	@Override
	public boolean ping() {
		return true;
	}

	@Override
	public void key(int key) {
		this.key = key;
	}

	@Override
	public AnswerMessage answer(AnswerMessage message) throws TException {
		String data = message.data;
		System.out.println("Message: " + data);
		String decrypted = AnswerMessageEncryption.decrypt(data, key);
		System.out.println("Decrypted message: " + decrypted);
		String response = response(decrypted);
		System.out.println("Response: " + response);
		String encrypted = AnswerMessageEncryption.encrypt(response, key);
		System.out.println("Encrypted response: " + encrypted + "\n");
		return new AnswerMessage(message.id, encrypted);
	}

	private String response(String message) throws UnanswerableException {
		switch (message) {
			case "The Ultimate Question of Life, the Universe, and Everything.":
				return "42.";
			case "Hello there.":
				return "General Kenobi...";
			case "It's over Anakin! I have the high ground.":
				return "You underestimate my power!";
			default:
				throw new UnanswerableException(22, "Invalid argument: Question Unanswerable.");
		}
	}
}