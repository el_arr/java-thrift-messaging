namespace java com.el_arr.thrift

typedef i32 int

exception UnanswerableException {
	1: int code,
	2: string desc
}

struct AnswerMessage {
	1: int id,
	2: string data
}

service AnswerService {

	bool ping(),

	oneway void key(1:int key),

	AnswerMessage answer(1:AnswerMessage message) throws (1:UnanswerableException e)

}